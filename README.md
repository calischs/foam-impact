This is documentation for dynamic impact testing infrastructure, intended to measure peak acceleration and energy return.

This project has was developed by Sam Calisch at MIT's Center for Bits and Atoms.  It has also been used by Nike, Inc. and by students in MIT's 3.042 Materials Science Project Laboratory.

The specs for the test are a mass of 7.8 kg, traveling at 1 m/s, with an circular impact area of diameter of 4 cm.

<img src='impactor/release-design.png' height=220px>
<img src='impactor/testing.jpg' height=220px>
<img src='video/tracker.png' height=220px>

<a href='https://gitlab.cba.mit.edu/calischs/foam-impact/tree/master/impactor'>impactor/</a> contains design files for a release mechanism for the mass.  This is built around a solenoid and a push-button quick release pin (90985A311 from McMaster-Carr).

<a href='https://gitlab.cba.mit.edu/calischs/foam-impact/tree/master/pcb'>pcb/</a> contains design files and firmware for an embedded logging system using the ADXL372Z accelerometer and the Cortex M0 Adalogger board for Micro SD Card integration and battery management.

<a href='https://gitlab.cba.mit.edu/calischs/foam-impact/tree/master/fw'>fw/</a> contains embedded code for detecting impact, sampling, and writing to the SD card.

<a href='https://gitlab.cba.mit.edu/calischs/foam-impact/tree/master/analysis'>analysis/</a> contains an ipython notebook for analyzing the acceleration profiles, extracting peak acceleration levels and energy dissipation.

<a href='https://gitlab.cba.mit.edu/calischs/foam-impact/tree/master/video'>video/</a> contains details of video analysis of the drop test.  For now, this is implemented with a Sony RX-100 IV camera shooting at 960 fps and the free software <a href='http://physlets.org/tracker/'>Tracker</a> to analyze acceleration profiles.

<img src='impactor/turning-mass.jpg' height=300px>
<img src='impactor/milling-mass.jpg' height=300px>

<img src='impactor/adxl372z.jpg' height=300px>
<img src='impactor/instron.jpg' height=300px>

<img src='analysis/img/asker-trajectories.png' height=300px>
<img src='analysis/img/asker-energies.png' height=300px>


I've been using this measurement device to characterize the dynamics of folded foam samples I've been producing.  Below are the acceleration profiles of two folded samples compared against conventional foams.

<img src='analysis/impact-first-kiri.png' width=80%>


I'm now working on a second version of this test apparatus which is self-contained (not mounted on an Instron).  It can be fabricated using a CNC router, a 3D printer, and a drill press or milling machine for a drilling and tapping a few holes.

<img src='v2/v2-screenshot.png' width=250px>
<img src='v2/routing-plates.jpg' width=450px>

<img src='v2/wox-parts.jpg' width=370px>
<img src='v2/bullet-drill-pattern.jpg' width=370px>

<img src='v2/impactor-v2.jpg' width=500px>



