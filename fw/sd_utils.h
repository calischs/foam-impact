
bool init_sd_card(){
  // see if the card is present and can be initialized:
  if (!SD.begin(cardSelect)) {
    if (debug) Serial.println("Card init. failed!");
    return 0;
  }
  char filename[15];
  strcpy(filename, "ADXL00.TXT");
  for (uint8_t i = 0; i < 100; i++) {
    filename[4] = '0' + i/10;
    filename[5] = '0' + i%10;
    // create if does not exist, do not open existing, write, sync after write
    if (! SD.exists(filename)) {
      break;
    }
  }
  logfile = SD.open(filename, FILE_WRITE);
  if( ! logfile ) {
    if (debug) {
      Serial.print("Couldnt create "); 
      Serial.println(filename);
      return 0;
    }
  }
  if (debug) Serial.print("Writing to "); 
  if (debug) Serial.println(filename); 
  return 1;
}
