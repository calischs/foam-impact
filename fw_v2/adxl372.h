
//adxl registers
const byte adxl_partid = 0x02;
const byte adxl_status = 0x04;
const byte adxl_status2 = 0x05;
const byte adxl_activity_status = 0x05;
const byte adxl_x_data = 0x08;
const byte adxl_y_data = 0x0A;
const byte adxl_z_data = 0x0C;

const byte adxl_offset_x = 0x20;
const byte adxl_offset_y = 0x21;
const byte adxl_offset_z = 0x22;

const byte adxl_act_x_l = 0x24;
const byte adxl_act_z_h = 0x27;
const byte adxl_act_z_l = 0x28;
const byte adxl_time_act = 0x29;

const byte adxl_hpf = 0x38;
const byte adxl_int1_map = 0x3B;
const byte adxl_int2_map = 0x3C;
const byte adxl_timing = 0x3D;
const byte adxl_measure = 0x3E;
const byte adxl_power_ctl = 0x3F;
const byte adxl_self_test = 0x40;

byte readbyte(byte address) {
  byte result;
  digitalWrite(SS_ADXL, LOW);
  SPI.transfer( (address << 1) | 0b000000001 );
  result = SPI.transfer(0x00);
  digitalWrite(SS_ADXL, HIGH);
  return (result);
}
void writebyte(byte address, byte value) {
  digitalWrite(SS_ADXL, LOW);
  SPI.transfer( (address << 1) & 0b11111110 );
  SPI.transfer(value);
  digitalWrite(SS_ADXL, HIGH);
}

bool init_adxl372(){
  //ADXL372
  // turn on full bandwidth measurement mode
  writebyte(adxl_power_ctl, 0b00000011);
  // enable low noise mode, 3200Hz output signal bandwidth
  writebyte(adxl_measure, 0b00001100);
  // configure ODR = 6400 Hz
  writebyte(adxl_timing, 0b10000000);
  // configure corner frequency 0 (30Hz at 6400Hz ODR)
  writebyte(adxl_hpf, 0b00000000);
  // set z trim
  writebyte( adxl_offset_z, 0b00000110 );

  
  delay(100); //start up
  
  writebyte(adxl_self_test, 0b00000001); //assert self test
  delay(300); //delay 300ms
  byte st = readbyte(adxl_self_test); //read self test status bits
  if ((st & 0b00000010) && (st & 0b00000100 )){
    Serial.println("Self test passed!");
  } else {
    Serial.print(st);
    Serial.println(", Self test failed!");
    return 0;
  }
  writebyte(adxl_self_test, 0b00000000); //clear self test

  // keep on full bandwidth measurement mode
  // disable lpf and hpf
  writebyte(adxl_power_ctl, 0b00001111);

  //configure motion warning on z axis
  writebyte(adxl_act_x_l, 0b00000010); //set referenced activity processing mode
  writebyte(adxl_act_z_h, 0b00000011); //set msb of threshold
  writebyte(adxl_act_z_l, 0b11100001); //enable z axis contribution to motion and set lsb of threshold
  writebyte(adxl_time_act, 0b00000010); //set activity time counter threshold
  writebyte(adxl_int1_map, 0b00100000 ); //map motion warning to int1 pin

  //configure data ready on int2 pin
  writebyte(adxl_int2_map, 0b00000001);

  // give the sensor time to set up:
  delay(100);
  return 1;
}

void shutdown_adxl372(){
  writebyte(adxl_power_ctl, 0b00000000);
}

